<?xml version="1.0" encoding="UTF-8"?>
<collection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:noNamespaceSchemaLocation="https://gitlab.com/Kerenoc/GCstar/raw/Test/gcstar/share/gcstar/schemas/gcm-revision5.xsd"
name="GCbuildingtoys">

	<lang>GCbuildingtoys</lang>
	<options>

		<defaults>
			<image>no.png</image>
			<groupby>brand</groupby>
		</defaults>

		<fields>
			<id>id</id>
			<title>name</title>
			<cover>mainpic</cover>
			<url>web</url>
			<play>instructions</play>
			<search>
				<field>reference</field>
				<field>name</field>
			</search>
			<results>
				<field>reference</field>
				<field>name</field>
				<field>mainpic</field>
				<field>nbpieces</field>
				<field>brand</field>
				<field>theme</field>
				<field>released</field>
			</results>
			<summary>
				<field>reference</field>
				<field>brand</field>
				<field>theme</field>
				<field>released</field>
				<field>tags</field>
			</summary>
		</fields>

		<overlay>
                        <image>subtle.png</image>
			<paddingLeft>12</paddingLeft>
			<paddingRight>11</paddingRight>
			<paddingTop>12</paddingTop>
			<paddingBottom>0</paddingBottom>
		</overlay>

		<values id="favouriteYesno">
			<value displayed="">0</value>
			<value displayed="PanelFavourite">1</value>
		</values>
	</options>

	
	<groups>
		<group id="info" label="SetDescription"/>
		<group id="details" label="Details"/>
		<group id="perso" label="Personal"/>
		<group id="pictures" label="Pictures"/>
		<group id="minifigs" label="Minifigures"/>
	</groups>

	<fields lending="false" tags="true">
		<field value="id"
				type="number"
				label="Id"
				init=""
				group="info"
				imported="false"/>
		<field value="reference"
				type="number"
				label="Reference"
				init=""
				group="info"
				imported="true"/>
		<field value="name"
				type="short text"
				label="Name"
				init="NewItem"
				group="info"
				imported="true"/>
		<field value="mainpic"
				type="image"
				label="MainPic"
				init=""
				group="info"
				imported="true"/>
		<field value="brand"
				type="history text"
				label="Brand"
				init=""
				group="info"
				imported="true"/>
		<field value="theme"
				type="history text"
				label="Theme"
				init=""
				group="info"
				imported="true"/>
		<field value="released"
				type="date"
				label="Released"
				init=""
				group="info"
				imported="true"
				sorttype="date"/>
		<field value="nbpieces"
				type="number"
				label="NumberPieces"
				init=""
				group="info"
				imported="true"
				sorttype="date"/>
		<field value="nbfigs"
				type="number"
				label="NumberMiniFigs"
				init=""
				group="info"
				imported="true"
				sorttype="date"/>
		<field value="description"
				type="long text"
				label="Description"
				init=""
				group="info"
				imported="true"/>
		<field value="web"
				type="button"
				format="url"
				label="Url"
				init=""
				group="info"
				imported="true"/>
		<field value="rating"
				type="number"
				displayas="graphical"
				label="PanelRating"
				init="0"
				max="10"
				group="details"
				imported="false"/>
		<field value="box"
				type="yesno"
				label="Box"
				init="1"
				group="details"
				notnull="true"
				imported="false"/>
		<field value="manual"
				type="yesno"
				label="Manual"
				init="1"
				group="details"
				notnull="true"
				imported="false"/>
		<field value="sealed"
				type="yesno"
				label="Sealed"
				init="1"
				group="details"
				notnull="true"
				imported="false"/>
		<field value="nbowned"
				type="number"
				label="NbOwned"
				min="0"
				max="1000"
				init="1"
				group="details"
				imported="false"/>
		<field value="added"
				type="date"
				label="PanelAdded"
				init="current"
				group="details"
				imported="false"/>
		<field value="location"
				type="history text"
				label="PanelLocation"
				init=""
				group="details"
				imported="false"/>
		<field value="paid"
				type="number"
				label="PaidPrice"
				init=""
				group="details"
				imported="false"/>
		<field value="estimate"
				type="number"
				label="EstimatedPrice"
				init=""
				group="details"
				imported="false"/>
		<field value="comments"
				type="long text"
				label="Comments"
				init=""
				group="details"
				imported="false"/>
		<field value="images"
				type="images"
				label="Images"
				init=""
				group="pictures"
				itemsPerRow="2"
				pictureHeight="200"
				imported="true"/>
		<field value="minifigpics"
				type="images"
				label="MinifiguresPics"
				init=""
				group="minifigs"
				itemsPerRow="6"
				pictureHeight="150"
				imported="true"/>
		<field value="instructions"
				type="file"
				format="ebook"
				label="Instructions"
				init=""
				group="details"
				imported="false"/>
	</fields>

	
	<filters>
		<group label="SetDescription">
			<filter field="name" comparison="contain"/>
			<filter field="brand" comparison="eq" quick="true"/>
			<filter field="theme" comparison="eq" quick="true"/>
		</group>
		<group label="Details">
			<filter field="box" comparison="eq" quick="true"/>
			<filter field="sealed" comparison="eq" quick="true"/>
			<filter field="manual" comparison="eq" quick="true"/>
		</group>
	</filters>


	<panels>
		<panel name="form" label="PanelForm" editable="true">
			<item type="line">
				<item type="value" for="reference" width="10" tip="" nomargin="true" />
				<item type="value" for="name" expand="true" nomargin="true" />
				<item type="special" for="searchButton" nomargin="true" />
				<item type="special" for="refreshButton" nomargin="true" />
			</item>
			<item type="notebook" expand="true">
				<item type="tab" value="info" title="SetDescription">
					<item type="line">
						<item type="box">
							<item type="value" for="mainpic" width="200" height="200"/>
						</item>
						<item type="table" title="Information" rows="6" cols="5">
							<item type="label" for="brand" row="0" col="0"/>
							<item type="value" for="brand" row="0" col="1"/>
							<item type="label" for="theme" row="1" col="0"/>
							<item type="value" for="theme" row="1" col="1"/>
							<item type="label" for="nbpieces" row="2" col="0"/>
							<item type="value" for="nbpieces" row="2" col="1"/>
							<item type="label" for="nbfigs" row="3" col="0"/>
							<item type="value" for="nbfigs" row="3" col="1"/>
							<item type="label" for="released" row="4" col="0"/>
							<item type="value" for="released" row="4" col="1"/>
						</item>
					</item>
					<item type="box" expand="true">
						<item type="label" for="description"/>
						<item type="value" for="description" expand="true"/>
					</item>
				</item>
				<item type="tab" value="details" title="Details">
					<item type="table" rows="7" cols="6">
						<item type="line" row="0" col="0" colspan="10">
						    <item type="value" for="box" />
							<item type="value" for="sealed" />
						    <item type="value" for="manual" />
					    </item>
						<item type="label" for="rating" row="1" col="0"/>
						<item type="value" for="rating" row="1" col="1" colspan="1"/>
						<item type="label" for="nbowned" row="1" col="4"/>
						<item type="value" for="nbowned" row="1" col="5" colspan="1"/>
						<item type="label" for="added" row="2" col="0"/>
						<item type="value" for="added" row="2" col="1" colspan="1"/>			
						<item type="label" for="location" row="3" col="0"/>
						<item type="value" for="location" row="3" col="1" colspan="1"/>
						<item type="label" for="paid" row="2" col="4"/>
						<item type="value" for="paid" row="2" col="5" colspan="1"/>			
						<item type="label" for="estimate" row="3" col="4"/>
						<item type="value" for="estimate" row="3" col="5" colspan="1"/>
						<item type="label" for="instructions" row="4" col="0"/>
						<item type="line" row="4" col="1" colspan="1">						
    						<item type="value" for="instructions" nomargin="true" expand="true"/>						
    					    <item type="launcher" for="instructions" nomargin="true"/>
					    </item>
					</item>
					<item type="table" rows="2" cols="2" expand="true">
						<item type="label" for="comments" row="1" col="0" colspan="2"/>
						<item type="value" for="comments" row="2" col="0" colspan="2" expand="true"/>
					</item>
				</item>
				<item type="tab" value="minifigures" title="Minifigures">
					<item type="value" for="minifigpics" expand="true"/>
				</item>
				<item type="tab" value="pictures" title="Pictures">
					<item type="value" for="images" expand="true"/>
				</item>
				<item type="tab" value="tagpanel" title="PanelTags">
					<item type="line">
						<item type="value" for="favourite" />
					</item>
					<item expand="true" for="tags" type="value" />
				</item>
			</item>
			<item type="line" homogeneous="true">
				<item type="value" for="web" expand="true"/>
				<item type="special" for="deleteButton" expand="true"/>
			</item>
		</panel>
	</panels>

</collection>
